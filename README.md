# Basic LAMP/Js working example


## This example demonstrates functional (non-OOP) PHP core functions and latest stable jQuery.


### To run on your local LAMP stack:

1. Checkout a new working copy to a directory accessible by Apache

2. Run [https://bitbucket.org/satyrwilder/lamp-js-basic/src/fbf93412accbca8914c142f8b35a03c464a350fe/airport-new.sql?at=master](airport-new.sql) in mysql

3. Start Apache, and navigate to [http://localhost:80/print.php](http://localhost:80/print.php)
